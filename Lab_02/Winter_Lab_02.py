'''
@file Winter_Lab_02.py

@brief Lab 2 for Mechatronics in Winter Quarter 
@details This lab was an introduction to using interupts. We explore the use 
of interupts by way of developing a reaction time test for the user. This is 
also the first time in ME 405 that we are using a timer

A link to the code is provided at : https://bitbucket.org/spinb17/me405/src/master/Lab_02/Winter_Lab_02.py

@author Ben Spin
@date 1/23/2021

'''
import pyb
import utime
import urandom

# Setting up the LED flasher as pin A5
my_led = pyb.Pin(pyb.Pin.board.PA5 , mode=pyb.Pin.OUT_PP)

# Setting timer up as pin 2 with a prescaler of 80 and the maximum period
tim = pyb.Timer(2, prescaler=(79) , period=0x7FFFFFFF)

# Setting original wait level to 0 secconds
wait = 0

tim.counter()

# Average wait time to 0
avg_tim = 0

# Counter for number of interupts developed 
counter = 0


# We are now setting up our interupt function for the reactoin timer
def reaction_timer(which_pin):
    # Two global intervals that we will need out of the reaction_timer
    global wait
    global counter
    # Setting up butt_press as a subtraction point from the counter
    wait += tim.counter() - butt_press
    counter += 1

# Interupt setup on the blue push button as per the instructions in Lab02    
extint = pyb.ExtInt (pyb.Pin.board.PC13,    #Which Pin
                     pyb.ExtInt.IRQ_FALLING, #Interupt on falling edge
                     pyb.Pin.PULL_UP,       # Activate pullup resistor
                     reaction_timer)           # Interrupt service routine


print ('This is a reaction time tester. Press the blue button when the green light comes on. If you mash the ' 
       'button your time will be inaccurate and too large. Try for a fast time! The average human reaction time is .25 seconds.'
       ' Beat it if you can! Press CNTRL-C when you are finished and CNTRL-D to run again')

# Sleeping to allow user to read prompt
utime.sleep(4)

while True:
    try:
        # A random selection along a range of 2-3 seconds that the LED will wait
        delay = urandom.randrange(2000000 , 3000000 , 1)
        utime.sleep_us(delay)
        # Setting the led high again
        my_led.high()
        # Grabbing the time stamp at the point of button press
        butt_press = tim.counter()
        # Sleeping for 1 second while the light is on to register user inputs
        utime.sleep_ms(1000)
        # Turning the LED off again
        my_led.low()
        if tim.counter() >= 15000000 and counter<1:
            typerror = 'one' +2
        
    
    # If Control-C is pressed, this is sensed separately from the keyboard
    # module; it generates an exception, and we break out of the loop
    except KeyboardInterrupt:
            print('CNTRL-C Detected, Calculating your average speed'
                 ''
                 ''
                 '')
            
            if wait == 0:
                print('No entries detected , might be time for a nap')
            else:
                avg_tim = wait/counter
                
                if avg_tim > 250000:
                    string_time = str(avg_tim/1e6)
                    print('Your average reaction time was ' + string_time + ' seconds. That is slower than average for a human. '
                          'Why dont you try again?')
                    
                elif avg_tim == 250000:
                    print ('Your average reaction time was exactly average at 250000 microseconds. Go buy a lottery ticket!')
                    
                    
                else:
                    string_time = str(avg_tim/1e6)
                    print('Your average reaction time was ' + string_time + ' seconds. That is faster than average for a human. '
                          'Keep it Up!')
                          
            break
        
    except:
            print('No button pushes recognized')
            print('Code was idol for too long')
            break

        
        
