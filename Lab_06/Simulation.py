'''
@file Simulation.py

@brief This is the file holding a link to the PDF for Lab_06

@details In Lab 06 we took the hand calculations from Lab_05 and 
applied Jacobian Lineariztion to get a relationship convienient to our purposes.
 We took this relationship and developed several plots showing the results of our 
 simulated motion in a variety of starting positions. We also implemented 
 a simple controller with paramters defined in the Lab manual to control the position 
 of our ball and other important physical parameters like platform angles. More 
 detailed analysis of our process as well as plots displaying the aformentioned conditions
 are contained in the PDF linked below
    
@author Ben Spin , Patrick Ward
@date 2/11/2021

@brief All work and plots are contained within the pdf linked here: https://bitbucket.org/spinb17/me405/src/master/Lab_06/Lab_06_PDF.pdf
'''