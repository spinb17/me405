'''
@file Winter_Lab_01.py
Documentatoin for / use of Winter_Lab_01.py

Acess at https://bitbucket.org/spinb17/me405/src/master/Lab_01/Winter_Lab_01.py

@author Ben Spin
@date January 18 , 2021
'''

'''
Code to run the FSM (not the init state). This code initializes the program not the 
FSM
'''

import keyboard
import time
state= 0 

def getChange(price, payment):
    
    '''
    @brief Computes change using the method derived in HW1
    @param price Is the price in pennies of the object
    @param payment Is the payment as a tuple of currency used

    '''
    
    # Seperating payment out into individual change and summing pennies
    pennies = payment[0]
    pennies += (payment[1]*5)
    pennies += (payment[2]*10)
    pennies += (payment[3]*25)
    pennies += (payment[4]*100)
    pennies += (payment[5]*500)
    pennies += (payment[6]*1000)
    pennies += (payment[7]*2000)
    
    #Checking to see if we have enough money to pay for our item and 
    #alerting if not
    if pennies < price:
        print('Insufficient Money, Try again')
    
    # Conditional for enough pennies to cover item expense
    elif pennies >= price:
        
        # Calculating leftover change from payment
        change = pennies - price
        
        # Division by each of the change types availbe to check for ammount of change
        # We are utilizing the fact that int will always round down for this part
        # of the code
        twenties = int(change/2000)
        change +=  -twenties*2000
        
        tens = int(change/1000)
        change +=  -tens*1000
        
        fives = int(change/500)
        change +=  -fives*500
        
        ones = int(change/100)
        change +=  -ones*100
        
        quarters = int(change/25)
        change +=  -quarters*25
        
        dimes = int(change/10)
        change +=  -dimes*10
        
        nickles = int(change/5)
        change +=  -nickles*5
        
        pennies = int(change/1)
        change +=  -pennies*1
        
        # Assigning a tuple with all of our calculated values of change
        change_tup = (pennies, nickles, dimes, quarters, ones , fives, tens , twenties)
        
        # Kicking the tuple out so that it may be analyzed
        return change_tup
        
    # Error statement incase something really bizarre happens   
    else:
        print('Error')
    
        
    pass

def printWelcome():
    '''
    @brief Prints a Vendotron^TM^ welcome message with beverage prices
    '''
    print(' ')
    print(' ')
    print('Welcome to Vendotron!')
    print('You will love our prices! ')
    print('Press E at any time to eject your change!')
    
    pass

while True:
    ## Implementing FSM using a while loop and an if statement will
    ## run until user enters CNRTL-C
        
    
        if state == 0:
            customer_payment = None
            customer_input = None
            payment = [0,0,0,0,0,0,0,0]
            change = None
            n=0
            
            printWelcome()
            state = 1
            print ('Payments should be entered as 8 integer values representing currency, please enter them 1 at a time')
            print ('')
            print ('1st entry is pennies')
            print ('2nd entry is nickles')
            print ('3rd entry is dimes')
            print ('4th entry is quarters')
            print ('5th entry is dollars')
            print ('6th entry is fives')
            print ('7th entry is tens')
            print ('8th entry is twenties')
            
            
        if state == 1:
            
            ## Making sure customer is not hitting eject
            if keyboard.read_key() != 'e':
                ## Converting to an integer
                customer_payment = int(keyboard.read_key())
                ## Sleeping to not double count keys
                time.sleep(.2)
                
                ## Checking to make sure that input is an integer
                if customer_payment in [0,1,2,3,4,5,6,7,8,9]:
                    
                    ## Counter for number of inputs
                    n += 1
                    payment[n-1] = customer_payment
                    
                    ## Marking that entries are completed
                    if n == 8:
                        ## Visual ques for user
                        print(''
                              
                              '')
                        print('Value of {Input} recognized you have {charnum} values left'.format(charnum = str(8-n) , Input = str(customer_payment)))
                        print(''
                              
                              '')
                        ## Converting to a tuple
                        tup_pay = tuple(payment)
                        
                        
                        print( ('Payment:\n'
                                ' %d pennies\n'
                                ' %d nickles\n'
                                ' %d dimes\n'
                                ' %d quarters\n'
                                ' %d dollars\n'
                                ' %d fives\n'
                                ' %d tens\n'
                                ' %d twenties\n') %tup_pay)
                        
                        print('')
                        print('Enter C for Cuke ($1.00)')
                        print('Enter P for Popsi ($1.20)')
                        print('Enter S for Spryte ($0.85)')
                        print('Enter D for Dr.Pupper ($1.10)')
                        print('')
                        
                        state = 2
                    
                    else:
                        ## Visual ques for user
                        print(''
                              
                              '')
                        print('Value of {Input} recognized you have {charnum} values left'.format(charnum = str(8-n) , Input = str(customer_payment)))
                  
                        state = 1
                        pass
                else:
                    ## Non integer or E entry
                    print('Invalid Charecter, Try again')
                    pass
                        
            
            else: 
                ## E entry recognized
                print('Eject Recognized. Please come again!')
                time.sleep(.2)
                state = 0
                pass
            
                
            
            
            
        if state == 2:
            
            
            ## Recognizing customer drink choice  
            customer_input = keyboard.read_key()
            time.sleep(.2)
            
            ## Making sure customer uses a recognized charecter
            if customer_input in ['c','C','p','P','S','s','D','d']:
                state = 3 #on the next iteration the FSM will run state 1
            
            ## Customer wants change back and has hit eject
            elif customer_input in ['e','E']:
                print('Eject Recognized. Please come again!')
                state = 0
            
            else:
                print('Command not reccognized please try again')
                state = 2
                
                pass
                
        
        if state == 3:
            
            ## Cuke input
            if customer_input == 'c' or customer_input == 'C':
            
                drink_price = 100
                drink_choice = 'Cuke!'
            
            ## Popsi input
            elif customer_input == 'p' or customer_input == 'P':
                
                drink_price = 120
                drink_choice = 'Popsi!'
            
            ## Spryte input
            elif customer_input == 's' or customer_input == 'S':
                
                drink_price = 120
                drink_choice = 'Spryte!'
            
            ## Dr.Pupper input
            elif customer_input == 'd' or customer_input == 'D':
                
                drink_price = 120
                drink_choice = 'Dr.Pupper!'
            
            ## Running homework code to compute change
            change = getChange(drink_price, payment)
            print ('Your change is: ')
            print( (
                    ' %d pennies\n'
                    ' %d nickles\n'
                    ' %d dimes\n'
                    ' %d quarters\n'
                    ' %d dollars\n'
                    ' %d fives\n'
                    ' %d tens\n'
                    ' %d twenties\n') %change)
            print ('Enjoy your ' + drink_choice)
            print ('Vending ....')
            state = 0
        
        else:
            '''
            Error managemnet
            '''
            pass
        

                    

        