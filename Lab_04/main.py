# -*- coding: utf-8 -*-
"""
Created on Wed Feb 10 17:11:00 2021

@author: erick
"""
import pyb


# Debug variables
ID = 0
temp = 0

# Address for destination
# Need to format address
address = 0x18
# Create on bus 1
#   Bus 1: (SCL, SDA) = (X9, X10) = (PB6, PB7)
i2c = pyb.I2C(1)
# Initialize as master
i2c.init(pyb.I2C.MASTER, baudrate = 115200)
        
# Address will be in the form of 0011xxx
#   where xxx is set by the user as the slave
#   address
# In my configuration, set to b' 0011000
print(i2c.is_ready(address))

if ID == 1:
    manufacturerID = i2c.mem_read(2, address, 0x06)
    print(manufacturerID)
if temp == 1:
    reading = i2c.mem_read(2, address, 0x05)
    print(reading)