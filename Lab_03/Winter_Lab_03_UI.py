
'''
@file Winter_Lab_03_UI.py

@image html Lab_03_Winter_Step.png



@brief User interface for Lab 03 , taking input of G from the user and sending over the serial port

@details Sends letter g over the serial port and starts the process of data collection. Also processes
the values sent from main.py and converts to a step function graph. Additionally, this UI front end creates 
and saves a csv file

You can find this code at: https://bitbucket.org/spinb17/me405/src/master/Lab_03/Winter_Lab_03_UI.py


@author Ben Spin
@date 2/2/2021

'''

import serial
import matplotlib.pyplot as plt
import numpy as np


## Setting up serial port
ser = serial.Serial (port='Com3', baudrate=115200 , timeout=10)
print ('Enter G to Begin Data Collection')
## Hold value marking if a charecter has been entered
char_check = False 

while char_check == False:
    ## The entered command from the user
    data_start = input('Enter Command: ')
    if(data_start=='G' or data_start=='g'):
        ## Housing eventual return to me code
        char_check = True
        ## Writing Data Start
        ser.write(str(data_start).encode('ascii'))
        ## counter
        n=1
    else:
        print ('Enter G to begin data collection')
        

        


## Voltage   
v=[]
## Time
t=[]


## First Value is 0
value = 0
## Stripped Values are 0
values = 0
## Run number is 0
n = 0




while True:
    ## Direct Read
    value = ser.readline()
    ## Stripping value of special charecters
    change = value.decode()
    ## Splitting change at apostrophy point
    values = change.strip().split(',')
    
    ## Check if End statement has been printed
    if values != ['End']:
        ## Checking if value has been recognized
        try:
            ## Recognizing value in float
            volts=float(values[0])
        except ValueError:
            pass
        else:
            ## Appending voltage and time values
            v.append(volts)
            t.append(2e-3*n)
            n+=1                    
    else:
        break

## Plotting time vs Voltage
plt.plot(t,v,'go')
plt.ylabel('Voltage [V]')
plt.xlabel('Time [ms]')
plt.title('Button Press Step Function ')

ser.close()
np.savetxt('Voltage_v_Time.csv',[t,v],delimiter=',')
