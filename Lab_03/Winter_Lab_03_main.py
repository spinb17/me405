
'''
@file Winter_Lab_03_main.py


@brief main.py file running on the nucleo that interacts with the UI

@details Takes a letter g over the serial port and runs a timed analog to digital 
converter. This code is triggered when a jump in values is detected, then it 
send over the serial port a series of values as read from pin A0

You can find this code at:  https://bitbucket.org/spinb17/me405/src/master/Lab_03/Winter_Lab_03_main.py


@author Ben Spin
@date 2/2/2021

'''
## Importing important classes
import pyb
from pyb import UART
import array

## Setting up serial port communication
myuart = UART(2)

## Defining Pin A0 to read data off
pin = pyb.Pin.board.PA0

## Analog to digital on described pin
adc = pyb.ADC(pin)

## Timer 2 initialization
tim = pyb.Timer(2 , freq=500000)

## Defining a buffer array of 1500 values
buf = array.array('H',(0 for n in range (1500)))

## Code always runs
while True:
    
    ## Checks for anything waiting in the uart
    if myuart.any() != 0:
        
        ## Setting a variable "char" as recognized charecter in UART
        char = myuart.readchar()
        
        ## Is charecter g or G?
        if char == 71 or 103:
            
            while True:
                
                ## Condition met by pushed button
                if adc.read() <= 1:
                    
                    ## Condition met by rising edge
                    if adc.read() >1:
                        
                        ## Timed recording of values
                        adc.read_timed(buf,tim)
                        break
            
            ## Only recognizing good data sets, if less than 4000 it is likely that an error occured
            if buf[len(buf)-1]>=4000:
                ## Writing buffer values to the UART serial
                for n in range (0, len(buf)) :
                    myuart.write('{:}\r\n'.format(buf[n]*(3.3/4080)))
            ## Writing an End statement for when the code has been fully developed    
            myuart.write('End')
            
                
                