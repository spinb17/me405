'''

@file PrelimCalcs.py

@brief This file holds scans of the calculations defining the motion of the balancing plate. 

@details All these scans are done working with Patrick Ward. We performed 
the steps as outlined in the Lab_05 Handout for defining Leg Kinematics and Kinetics.
    
@author Ben Spin , Patrick Ward
@date 2/18/2021

@brief Schematic of the 4-bar Linkage OADQ.

@details We used this schematics and the small angle aproximaion to devlop a kinmatic reationships between the motion of 
the motor about A and the motion of the platform about O
@image html 1_1.jpg

@brief Kinmatics of Ball on Platform.

@details We developed a set of Kinematic equations that represent the  relationship
between the motion of the platform and the motion of the ball
@image html 2_1.jpg
@image html 2_2.jpg
@image html 2_3.jpg
@image html 2_4.jpg


@brief Equations of Motion for the System.

@details By summing moments at O, A , and the contact point for the ball we developed
equations of motions for the system. We then elimanted the unknown reaction force at point Q.
This was done by assuming the forces were purely vertical at this point, a result of the small 
angle approximation.
@image html 3_a_c.jpg

@brief Kinematics of G.

@details A brief bit of hang calcs outlining the development of Kinematic equations
for the center of mass of the plate
@image html 3_G_Kinematics.jpg

@brief Seperating the Unknowns.

@details We manipulated our equations such that the unknown accelerations and lower order terms 
were on the left hand side of our equations, and motor torques were all on the right.
@image html 4.jpg

@brief Manipulating Model.

@details This step of the hand calculations involved manipulating our seperated equations 
in the form outlined in the Lab_05 handout. This prepares us to enter numerical parameters and simulate
our motion.
@image html 5.jpg


'''



